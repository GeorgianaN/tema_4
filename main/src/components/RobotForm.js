import React,{Component} from 'react'
class RobotForm extends Component{
    constructor(props){
        super(props)
        this.state={
            type:'',
            name:'',
            mass:''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
       this.add=()=>{
           this.props.onAdd({
               type:this.state.type,
               name:this.state.name,
               mass:this.state.mass
           })
       }
    }
    render(){
        let {item}=this.props
        return(
        <div>
            <input id="type" type="text" placeholder="type" name="type" onChange={this.handleChange}/>
            <input id="name" type="text" placeholder="name" name="name" onChange={this.handleChange}/>
            <input id="mass" type="text" placeholder="mass" name="mass" onChange={this.handleChange}/>
            <input id="" type="button" value="add" onClick={this.add}/>
        </div>
        )}
}

export default RobotForm